$('.ui.rating')
  .rating({
    maxRating: 6,
  }
);


const getElemId = (name) => document.getElementById(name);

function toggleShow(element) {
  const {id} = element.currentTarget;
  getElemId(`${id}_modal`).classList.toggle('is-hidden')
}

getElemId('skills').addEventListener('click', toggleShow);

getElemId('projects').addEventListener('click', toggleShow);

getElemId('education').addEventListener('click', toggleShow);
