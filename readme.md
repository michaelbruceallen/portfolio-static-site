
* Set up the main structure with HTML5 semantics
* CSS Preprocessor Semantics-UI

![hero page](./journal/hero.png)
![](./journal/skills-section.png)
![](./journal/projects-section.png)
![hero page](./journal/hero-skills-view.png)

 Created a simple static view for skills section.

### Skills Section
 * I think a nice dynamic React view here is good
 * perhaps this should not be in a .ui.container
   * two columns and scrollable skills
 * The detailed Javascript view shows
   * about
   * stars and the reason
   * project list
     * a modal popup of Projects section
   * education list
     * a modal popup of education section

### Project Section
 * The dtailed view shows
   * About
   * Bullet points
   * Skills used
     * a modal popup of skills detail upon click
 * a modal pop up here would be nice.


### Education Section
 * The detailed view shows
    * About
    * Details
    * Skills Learned
